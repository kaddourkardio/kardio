[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://github.com/syl20bnr/spacemacs)


# kardio
kardio is a web application for management of a cardiology departement' patients. It is built with [Python][0] using the [Django Web Framework][1].

This project has the following basic apps:
- malades : for recording medical reports
- interventions: for reporting several interventions (angioplasty, pacing...)

## Installation
### Quick start
To set up a development environment quickly, first install Python 3. It comes with virtualenv built-in. So create a virtual env by:

```
1. `$ python3 -m venv kardio`
2. `$ . kardio/bin/activate`
3. cd kardio
4. git clone https://kaddourkardio@bitbucket.org/kaddourkardio/kardio.git
```

Install all dependencies:

```
pip install -r requirements.txt
```
Finally run the project
```
cd src
cp kardio/settings/local.sample.env kardio/settings/local.env 
python manage.py migrate

```



## Dependencies

Besides the python required packages, kardio depends on [XeTeX](https://ctan.org/pkg/xetex) that supports well unicode.



### TODO's

- **Fixed** 


fixing permissions using `django-braces` 



- using [reportlab](https://www.reportlab.com) for report creation as a drop in replacement for \LaTeX .
- Creating a dashboard for monitoring rooms .

### aknowledgements

[Arun Ravindran](http://arunrocks.com) for his django template (edge).

http://django-edge.readthedocs.org/

[0]: https://www.python.org/
[1]: https://www.djangoproject.com/