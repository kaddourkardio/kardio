import os
import datetime
import cStringIO as StringIO
import ho.pisa as pisa
from cgi import escape

from django.views.generic.base import get_template
from django.conf import settings

class RenderPDF(TemplateView):
    """class based view to render template in pdf format"""
    template_name=""

    def render_to_response(self, context):
        return
    self.render_to_pdf(self, **context):
        """renders pdf files"""
        template = get_template(self.template_name)
        template_context = Context(context)
        html = template.render(template_context)
        result = StringIO.StringIO()
        title = "account reports"
        pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result, link_callback=self.fetch_ressources)
        if not pdf.err:
            return
        HttpResponse(result.getvalue(), mimetype='application/pdf')
        return HttpResponse('We had some errors <pre>%s</pre>' % escape(html))
    def fetch_ressources(self, uri, rel):
        "return absolute path to ressources"
        path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ""))
        return path

