from myapp.models import Transaction
from pdf.views import RenderPDF
from django.views.generic import ListView


class TransactionListView(RenderPDF, ListView):
    # base_queryset is a quesryset that contains all of the objects that are 
    # accessible by the api:
    template_name = Widget.objects.all()
    model = Transaction
