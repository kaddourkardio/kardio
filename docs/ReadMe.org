#+TITLE: Django tips and tricks

- *mg src=""
alt="" align="left"
title="image title"
clas
#+END_TITLE


* Django braces

** TODO 
- documentation
- include PermissionMixins and GroupMixins
- export csv
*** code example

 #+BEGIN_SRC python 
 from braces import views
class SomeClassView(loginRequiredMixin, Listview):
class SomeClassView(loginRequiredMixin, StaffuserRequiredMixin, Listview):

class SomeProtectedView(GroupRequiredMixin, TemplateView):
    def get_group_required(self):
        # Get group or groups however you wish
        group = 'secret_group'
        return group

 #+END_SRC
* permission mixins

- LoginRequiredMixin
- StaffuserRequiredMixin
- GroupRequiredMixin

| date       | achat | vente |
|------------+-------+-------|
| 11/09/2015 |    34 |   147 |
|            |       |       |
|            |       |       |



  

