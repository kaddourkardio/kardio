#from reportlab.lib.pagesizes import A4
#from reportlab.lib.styles import getSampleStyleSheet
#from reportlab.pdfbase import pdfmetrics
#from reportlab.pdfbase.ttfonts import TTFont
#from reportlab.pdfgen import canvas
#from reportlab.platypus import Paragraph, Image
#from reportlab.lib.units import mm

# create the view function
#styles = getSampleStyleSheet()
#styles.add(ParagraphStyle(name='RightAlign', fontName='Minion Pro', align=TA_RIGHT))

#def rapport(self):
 #   buffer = self.buffer
  #  doc = SimpleDocTemplate(buffer,
   #                         rightMargin=15mm,
    #                        leftMargin=15mm,
     #                       topMargin=20mm,
      #                      bottomMargin=15mm,
       #                     pagesize=self.pagesize)
    # our container for flowable objects
  #  elements=[]


import time
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.lib.pagesizes import A4
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import mm
 
doc = SimpleDocTemplate("form_letter.pdf",pagesize=A4,
                        rightMargin=25,leftMargin=25,
                        topMargin=25,bottomMargin=28)
Story=[]
logo = "logo.png"
magName = "Cardiologie"
issueNum = 12
subPrice = "99.00"
limitedDate = "03/05/2010"
freeGift = "tin foil hat"
 
formatted_time = time.ctime()
full_name = "Dr Yahyaoui Mohamed Kaddour"
address_parts = ["EH Dr.Benzerdjeb", "Aïn Témouchent, 46000"]
 
im = Image(logo, 15*mm, 15*mm)
Story.append(im, 30, 450)
 
styles=getSampleStyleSheet()
styles.add(ParagraphStyle(name='Justify', alignment=TA_JUSTIFY))
ptext = '<font size=12>%s</font>' % formatted_time
 
Story.append(Paragraph(ptext, styles["Normal"]))
Story.append(Spacer(1, 12))
 
# Create return address
ptext = '<font size=12>%s</font>' % full_name
Story.append(Paragraph(ptext, styles["Normal"]))       
for part in address_parts:
    ptext = '<font size=12>%s</font>' % part.strip()
    Story.append(Paragraph(ptext, styles["Normal"]))   
 
Story.append(Spacer(1, 12))
ptext = '<font size=12>Dear %s:</font>' % full_name.split()[0].strip()
Story.append(Paragraph(ptext, styles["Normal"]))
Story.append(Spacer(1, 12))
 
ptext = '<font size=12>Bonjour et bienveue en  %s ! \
        vous êtes l\'heureux gagnant de  %s issues at the excellent introductory price of $%s.\
            %s to start receiving your subscription and get the following free gift: %s.</font>' % (magName, 
                                                                                                issueNum,
                                                                                                subPrice,
                                                                                                limitedDate,
                                                                                                freeGift)
Story.append(Paragraph(ptext, styles["Justify"]))
Story.append(Spacer(1, 12))
 
 
ptext = '<font size=12>Je reste à votre disposition et vous remercie chaleureusement de votre confiance.</font>'
Story.append(Paragraph(ptext, styles["Justify"]))
Story.append(Spacer(1, 12))
ptext = '<font size=12>Cordialement,</font>'
Story.append(Paragraph(ptext, styles["Normal"]))
Story.append(Spacer(1, 48))
ptext = '<font size=12>Tuxador</font>'
Story.append(Paragraph(ptext, styles["Normal"]))
Story.append(Spacer(1, 12))
doc.build(Story)

