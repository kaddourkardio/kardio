#class based or function based? It's really quite easy.
from django.views.generic import TemplateView

class MultipleModelView(TemplateView):
    template_name = 'template.html'

    def get_context_data(self, **kwargs):
         context = super(MultipleModelView, self).get_context_data(**kwargs)
         context['modelone'] = ModelOne.objects.get(*query logic*)
         context['modeltwo'] = ModelTwo.objects.get(*query logic*)
         return context



{% for object in modelone %}
{{ object.whatever }}
{% endfor %}

{% for object in modeltwo %}
{{ object.whatever }}
{% endfor %}
