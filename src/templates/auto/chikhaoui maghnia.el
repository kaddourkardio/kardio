(TeX-add-style-hook
 "chikhaoui maghnia"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt" "a4paper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "left=2cm" "right=2cm" "top=2cm" "bottom=2cm") ("xcolor" "usenames" "dvipsnames" "svgnames" "table")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art12"
    "fontspec"
    "xunicode"
    "graphicx"
    "geometry"
    "xltxtra"
    "polyglossia"
    "xcolor"
    "fancyhdr")
   (LaTeX-add-polyglossia-langs
    '("french" "defaultlanguage" "")
    '("arabic" "otherlanguage" ""))))

