from django.conf.urls import include, url, patterns
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
import profiles.urls
import accounts.urls
from . import views
from benzerdjeb import models
from benzerdjeb.views import (ListPatients, CreatePatient, DetailPatient, UpdatePatient, ListAdmissions, CurrentAdmissions, DetailAdmission, CreateAdmission, UpdateAdmission, ListBiologys, CreateBiology,  UpdateBiology, DetailBiology, admission_pdf, ListVisites, CreateVisite, DetailVisite, PatientSearchListView )
from intervention import models
from intervention.views import (ListCoronarographies, CreateCoronarographie, UpdateCoronarographie, DetailCoronarographie, UpdateCoronarographie, ListStimulations, DetailStimulation, CreateStimulation, UpdateStimulation
                                , entry_as_pdf, stimulation_pdf)

urlpatterns = [
    url(r'^accueil$', views.HomePage.as_view(), name='accueil'),
    url(r'^about/$', views.AboutPage.as_view(), name='about'),
    url(r'^users/', include(profiles.urls, namespace='profiles')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include(accounts.urls, namespace='accounts')),
    url('^markdown/', include( 'django_markdown.urls')),
    #url(r'^admission/', include(admission.urls, namespace='admissions'))
    url(r'^patient/(?P<pk>\d+)/edit/$', UpdatePatient.as_view(), name='update_patient'),
    url(r'^patient/(?P<pk>\d+)/$', DetailPatient.as_view(), name='detail_patient'),
#    url(r'^delete/(?P<pk>\d+)/$', DeletePatient.as_view(), name='delete_note'),
    url(r'^patients/$', ListPatients.as_view(), name='list_patients'),
    url(r'^patient/new/$', CreatePatient.as_view(), name='create_patient'),

    url(r'^admissions/$', ListAdmissions.as_view(), name='list_admissions'),
    url(r'^nouvelle_admission/$', CreateAdmission.as_view(), name='create_admission'),
    url(r'^admission/(?P<pk>\d+)/edit/$', UpdateAdmission.as_view(), name='update_admission'),
    url(r'^admission/(?P<pk>\d+)/$', DetailAdmission.as_view(), name='detail_admission'),
#    url(r'^bilans/$', ListBiologys.as_view(), name='list_biologys'),
    url(r'^bilan/new/$', CreateBiology.as_view(), name='create_biology'),
    url(r'^bilan/(?P<pk>\d+)/edit/$', UpdateBiology.as_view(), name='edit_biology'),
#    url(r'^bilan/(?P<pk>\d+)/delete/$', DeleteBiology.as_view(), name='delete_biology'),
    url(r'^bilan/(?P<pk>\d+)/$', DetailBiology.as_view(), name='detail_biology'),
    url(r'^bilans/$', ListBiologys.as_view(), name='list_biologys'),
    url(r'^coronarographies/$', ListCoronarographies.as_view(), name='list_coronarographies'),
    url(r'^coronarographie/(?P<pk>\d+)/edit/$', UpdateCoronarographie.as_view(), name='update_coronarographie'),
    url(r'^coronarographie/(?P<pk>\d+)/$', DetailCoronarographie.as_view(), name='detail_coronarographie'),
    url(r'^coronarographie/new/$', CreateCoronarographie.as_view(), name='create_coronarographie'),

    url(r'^stimulations/$', ListStimulations.as_view(), name='list_stimulations'),
    url(r'^stimulation/(?P<pk>\d+)/edit/$', UpdateStimulation.as_view(), name='update_stimulation'),
    url(r'^stimulation/(?P<pk>\d+)/$', DetailStimulation.as_view(), name='detail_stimulation'),
    url(r'^admission/admission(?P<pk>\d+)\.pdf$', admission_pdf, name='admission_pdf' ),
    url(r'^coronarographie/coro(?P<pk>\d+)\.pdf$', entry_as_pdf, name='rapport_coro'),

    url(r'^stimulation/new/$', CreateStimulation.as_view(), name='create_stimulation'),
    url(r'stimulation/pace(?P<pk>\d+)\.pdf$', stimulation_pdf, name='stimulation_pdf'),

    url(r'^visite/new/$', CreateVisite.as_view(), name='create_visite'),
    # url(r'^visite/(?P<pk>\d+)/edit/$', UpdateVisite.as_view(), name='edit_visite'),
#    url(r'^bilan/(?P<pk>\d+)/delete/$', DeleteBiology.as_view(), name='delete_biology'),
    url(r'^visite/(?P<pk>\d+)/$', DetailVisite.as_view(), name='detail_visite'),
    url(r'^visites/$', ListVisites.as_view(), name='list_visites'),
    url(r'^results/$', PatientSearchListView.as_view(), name='search'),

    url(r'^$', CurrentAdmissions.as_view(), name='home'),
  #   url(r'^admission_bis/(?P<pk>\d+)\.pdf$', secondview, name='admission_pdf' )
]
 #User-uploaded files like profile pics need to be served in development

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
