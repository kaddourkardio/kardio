from django.db import models
from datetime import datetime
from django.utils import timezone
from django.conf import settings
from django.core.urlresolvers import reverse

class   Wilaya(models.Model):
                wilaya = models.CharField(max_length=25)

                def __str__(self):
                    return(self.wilaya)


class   Motif(models.Model):
                motif = models.CharField(verbose_name="motif d'admission", max_length=25)

                def __str__(self):
                    return(self.motif)

class   Tag(models.Model):
                tag = models.CharField(max_length=50, blank=True)

                def __str__(self):
                    return(self.tag)


class   Patient(models.Model):
                name = models.CharField(verbose_name="nom et prénoms", max_length=50)
                birth = models.DateField(verbose_name="date de naissance")
                GENDER_CHOICES = (
                     ('M', 'Masculin'),
                     ('F', 'Féminin'),
                )
                gender = models.CharField(verbose_name="sexe", max_length=1, choices=GENDER_CHOICES)
                ardress = models.ForeignKey(Wilaya)
                phone = models.CharField("Téléphone", max_length=10, default='0550123456')
                assured = models.BooleanField(verbose_name="assuré social", default=True)
                correspondant = models.CharField("Médecin correspondant", max_length=255, blank=True)
                first_admission = models.DateField("première admission", default=timezone.now)
                hypertension = models.BooleanField("hypertendu(e)", default=True)
                diabete = models.BooleanField("diabétique", default=True)
                dyslipidemia = models.BooleanField("dyslipidémie", default=True)
                smoker = models.BooleanField("tabagique", default=False)
                weight = models.PositiveSmallIntegerField(verbose_name="poids", blank=True)
                lenght = models.DecimalField(max_digits=3, decimal_places=2, default=1.70)
                #bmi = weight/lenght*lenght
                obesity = models.BooleanField("obèse", default=True)
                sedentarity = models.BooleanField("sédentarité", default=True)
                heredity = models.BooleanField("hérédité coronarienne", default=False)
                history = models.TextField("antécedents")
                allergy = models.CharField("allergie connue", max_length=50, blank=True)
                tags = models.ManyToManyField(Tag, default="cardio")

                class Meta:
                    ordering = ['name']

                def get_absolute_url(self):
                    return reverse('detail_patient', kwargs={'pk': self.pk})

                def __str__(self):
                    return(self.name)





class   Admission(models.Model):

# données générales et symptomatologie

                patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
                admission_date = models.DateField("date d'admission")
                medecin = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True)
                emergency = models.BooleanField("urgence", default=False)
                motif_admission = models.ManyToManyField(Motif)
                fever = models.BooleanField("fièvre", default=False)
                dyspnea_nyha = models.PositiveSmallIntegerField("stade NYHA", default='1')
                angina_scc = models.PositiveSmallIntegerField("stade d'angor", default='1')
                syncope = models.BooleanField("Syncope", default=False)
                histoire = models.CharField("Histoire de la maladie", max_length=255 ,null=True, blank=True)

# examen clinique

                systolic_bp = models.PositiveSmallIntegerField(blank=True)
                diastolic_bp = models.PositiveSmallIntegerField(blank=True)
                heart_rate = models.PositiveSmallIntegerField(blank=True)
                auscultation = models.CharField(max_length=50, blank=True)
                ivg = models.BooleanField(default=False)
                ivd = models.BooleanField(default=False)
                pulse = models.CharField(max_length=50, blank=True)
                telethorax = models.CharField(max_length=50, blank=True)

                # ECG

                RYTHM_CHOICES = (
                    ('S', 'Sinusal'),
                    ('F', 'Atrial Fibrillation'),
                    ('R', 'Atrial Flutter'),
                    ('P', 'Pace Maker'),
                    ('T', 'TV')
                )
                rythm = models.CharField(max_length=1, choices=RYTHM_CHOICES)
                freq = models.PositiveSmallIntegerField(default='70')
                BLOCK_CHOICES = (
                    ('D', 'BBD'),
                    ('G', 'BBG'),
                    ('N', 'Null')
                    )
                bundle_block = models.CharField(max_length=1, choices=BLOCK_CHOICES, blank=True)

                hypertrophy = models.NullBooleanField(default=False)
                TERRITORY = (
                    ('N', 'Null'),
                    ('S', 'Septal'),
                    ('A', 'Anterior'),
                    ('L', 'Lateral'),
                    ('P', 'Posterior'),
                )
                necrosis = models.CharField(max_length=1, choices=TERRITORY)
                ischaemia = models.CharField(max_length=1, choices=TERRITORY)
                lesion = models.CharField(max_length=1, choices=TERRITORY)
                t_inversion = models.CharField(max_length=1, choices=TERRITORY)
                corrected_qt = models.IntegerField(default=400)
#echocoeur
                echocoeur = models.TextField("échocardiographie", null=True, blank=True)
                date_echo = models.DateField("Date de l'écho", default=timezone.now)
                eto = models.TextField("échocardiographie trans-oesophagienne", null=True, blank=True)

                fevg = models.PositiveSmallIntegerField("FeVG", blank=True, null=True)

#evolution

                resume = models.TextField("résumé clinique", null=True)
                evolution = models.CharField("évolution de la maladie", max_length=255, blank = True, null=True)

# ordonnance de sortie

                beta_blocker = models.BooleanField("béta bloquant", default=True)
                ace = models.BooleanField("inhibiteur de l'enzyme de conversion", default=True)
                plavix = models.BooleanField(default=True)
                aspegic = models.BooleanField(default=True)
                statin = models.BooleanField("statine", default=True)
                heparin = models.BooleanField("héparines", default=True)
                furosemid = models.BooleanField("furosemide", default=True)
                aldactone = models.BooleanField(default=True)
                nitrine = models.BooleanField("dérivés nitrés", default=False)
                insulin = models.BooleanField("insuline", default=False)
                oral_anti_diabetic = models.BooleanField("anti diabétiques oraux", default=True)
                avk = models.BooleanField("AVK", default=False)
# discharge
                date_sortie = models.DateField(blank=True, null=True)
                dispositions = models.CharField("dispositions complémentaires", max_length=255, blank=True)
# mesurer la durée d'hospitalisation à la django!

                duration = models.DurationField("durée de l'hospitalisation", blank=True)

                def save(self, *args, **kwargs):
                                if self.date_sortie:
                                                self.duration = self.date_sortie - self.admission_date
                                else:
                                                self.duration = datetime.now().date() - self.admission_date

                                super(Admission, self).save(*args, **kwargs)

#                EVOLUTION_CHOICES = (
#                    ('F', 'Favorable'),
#                    ('S', 'Stable'),
#                    ('G', 'Aggravation'),
#                    ('D', 'Décès'),
#                )
#                evolution_items = models.CharField(max_length=1, choices=EVOLUTION_CHOICES)
                treatement = models.CharField("ordonnance de sortie", max_length=100)
                sortant = models.BooleanField(default=False)

                def __str__(self):

                    return '%s %s' % (self.patient, self.admission_date)
                def get_absolute_url(self):
                    return reverse('detail_admission', kwargs={'pk': self.pk})

class Biology(models.Model):
                patient = models.ForeignKey(Patient)
                prelevement = models.DateField(blank=True, default = timezone.now)
# Biologie
                glycemia = models.DecimalField(max_digits=4, decimal_places=2, default=1.00)
                creatinin = models.DecimalField(max_digits=5, decimal_places=2, default=9)
                mdrd = models.PositiveSmallIntegerField()
                troponin = models.PositiveSmallIntegerField("taux de troponines/ normale", default=1)
                cholesterol = models.DecimalField(max_digits=3, decimal_places=2, default=1.00)
                triglyceride = models.DecimalField(max_digits=3, decimal_places=2, default=1.00)
                hdl = models.DecimalField(max_digits=3, decimal_places=2, default=1.00)
                ldl = models.DecimalField(max_digits=3, decimal_places=2, default=1.00)

                def __str__(self):
                    return '%s %s' % (self.patient, self.prelevement)
                def get_absolute_url(self):
                    return reverse('detail_biology', kwargs={'pk': self.pk})


class   Visite(models.Model):

# données générales et symptomatologie

                patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
                visite_date = models.DateField("date de la visite", default=timezone.now)
                medecin = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True)

                fever = models.BooleanField("fièvre", default=False)
                dyspnea_nyha = models.PositiveSmallIntegerField("stade NYHA", default='1')
                angina_scc = models.PositiveSmallIntegerField("stade d'angor", default='1')
                syncope = models.BooleanField("Syncope", default=False)
                # examen clinique

                systolic_bp = models.PositiveSmallIntegerField("PAS", blank=True)
                diastolic_bp = models.PositiveSmallIntegerField("PAD", blank=True)
                heart_rate = models.PositiveSmallIntegerField("FC", blank=True)
                auscultation = models.CharField("Auscultation", max_length=50, blank=True)
                ivg = models.BooleanField(default=False)
                ivd = models.BooleanField(default=False)
                pulse = models.CharField("Pouls", max_length=50, blank=True)
                ecg = models.CharField("ECG", max_length=50, blank=True)
                bio = models.CharField(max_length=50, blank=True)
                examens = models.CharField("Examens complémentaires", max_length=50, blank=True)
                traitement = models.CharField("Traitement", max_length=100)
                todo = models.CharField("à faire",max_length=50, blank=True)

                def __str__(self):
                    return '%s %s' % (self.patient, self.visite_date)
                def get_absolute_url(self):
                    return reverse('detail_visite', kwargs={'pk': self.pk})


