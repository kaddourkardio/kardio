import os
from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy
from benzerdjeb.models import Patient, Admission, Biology, Visite
from intervention.models import Coronarographie, Stimulation
from django.views.generic import ListView, DetailView, TemplateView, CreateView, UpdateView, DeleteView
#from benzerdjeb.forms import PatientForm, AdmissionForm
from django.http import HttpResponse
from django.shortcuts  import render_to_response,redirect
from django.template import Context
from django.template.loader import get_template, render_to_string
# from .utils import generic_search
from braces.views import LoginRequiredMixin, GroupRequiredMixin
from subprocess import Popen, PIPE
import tempfile
#from tempfile import mkstemp
#from io import BytesIO
import operator
from django.db.models import Q
from six.moves import reduce
#from vanilla import TemplateView, DetailView, CreateView, DeleteView, ListView, UpdateView

class HomePage(TemplateView):
    template_name = "home.html"


class AboutPage(TemplateView):
    template_name = "about.html"


class ListPatients(LoginRequiredMixin, ListView):
    model = Patient
    context_object_name = 'patients'
#    def patients(self):
        #template_name = "benzerdjeb/patient_list.html"
        #return Patient.objects.order_by(-first_admission)



class DetailPatient(LoginRequiredMixin, DetailView):
    model = Patient
    context_object_name = 'patient'

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(DetailPatient, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['admissions'] = Admission.objects.filter(patient=self.object)
        context['biologies'] = Biology.objects.filter(patient=self.object)
        context['coronarographies'] = Coronarographie.objects.filter(patient=self.object)
        context['stimulations'] = Stimulation.objects.filter(patient=self.object)
        return context



class CreatePatient(LoginRequiredMixin, CreateView):
    model = Patient
    fields = '__all__'
#    group_required = u'Cardiologues'
#    form = PatientForm
#    success_url = reverse_lazy('detail_patient')
    success_url = reverse_lazy('list_patients')

class UpdatePatient(LoginRequiredMixin, UpdateView):
    model = Patient
    fields = '__all__'
#    group_required = u'Cardiologues'
#    form = PatientForm
#    success_url = reverse_lazy('detail_patient')
    success_url = reverse_lazy('list_patients')



class ListAdmissions(LoginRequiredMixin, ListView):
    model = Admission
    context_object_name = 'admissions'


class CurrentAdmissions(LoginRequiredMixin, ListView):
    model = Admission
    context_object_name = 'admissions'
    template_name = 'benzerdjeb/admission_current.html'
    def get_queryset(self):
        return Admission.objects.filter(sortant=False)



class DetailAdmission(LoginRequiredMixin, DetailView):
    model = Admission
    context_object_name = 'admission'
    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(DetailAdmission, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        #context['biologies'] = Biology.objects.filter(patient=self.object)
        return context


#class IndexView(ListView):
#   context_object_name = 'home_list'
#   template_name = 'contacts/index.html'
#   queryset = Individual.objects.all()

#   def get_context_data(self, **kwargs):
#       context = super(IndexView, self).get_context_data(**kwargs)
#       context['roles'] = Role.objects.all()
#       context['venue_list'] = Venue.objects.all()
#       context['festival_list'] = Festival.objects.all()
    # And so on for more models
#       return context
        
class CreateAdmission(GroupRequiredMixin, CreateView):
    model = Admission
    fields = '__all__'
    group_required = u'Cardiologues'
    #form = AdmissionForm
    success_url = reverse_lazy('list_admissions')

class UpdateAdmission(GroupRequiredMixin, UpdateView):  
    model = Admission
    fields = '__all__'
    group_required = u'Cardiologues'
#    form = PatientForm
#    success_url = reverse_lazy('detail_patient')
    success_url = reverse_lazy('list_admissions')


class ListBiologys(LoginRequiredMixin, ListView):
    model = Biology

    context_object_name = 'biology_list'

class DetailBiology(LoginRequiredMixin, DetailView):
    model = Biology
    context_object_name = 'biologie'
    def get_context_data(self, **kwargs):
    # Call the base implementation first to get a context
        context = super(DetailBiology, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['patient'] = Patient.objects.filter(name=self.object)
        return context


class CreateBiology(LoginRequiredMixin, CreateView):
    model = Biology
    fields = '__all__'
    success_url = reverse_lazy('biology_list')

class UpdateBiology(LoginRequiredMixin, UpdateView):
    model = Biology
    fields = '__all__'
#    success_url = reverse_lazy('biology_list')

class DeleteBiology(GroupRequiredMixin ,DeleteView):
    model = Biology
    fields = '__all__'
    group_required = u'Cardiologues'
    success_url = reverse_lazy('biology_list')


    # je vais essayer une function based views, puis au fur et à mesure la convertir vers CBV




def admission_pdf(request, pk):
    entry = Admission.objects.get(pk=pk)
    source = Patient.objects.get(pk=pk)
    context = Context({ 'admission': entry, 'patient': source })
    # buffer = BytesIO()  
    template = get_template('benzerdjeb/report.tex')
    rendered_tpl = template.render(context, request).encode('utf-8')
    # Python3 only. For python2 check out the docs!
    with tempfile.TemporaryDirectory() as tempdir:
        # Create subprocess, supress output with PIPE and
        # run latex twice to generate the TOC properly.
        # Finally read the generated pdf.
        for i in range(2):
            process = Popen(
                ['xelatex', '-output-directory', tempdir],
                stdin=PIPE,
                stdout=PIPE,
            )
            process.communicate(rendered_tpl)
        with open(os.path.join(tempdir, 'texput.pdf'), 'rb') as f:
            pdf = f.read()
    r = HttpResponse(content_type='application/pdf')
    # r['Content-Disposition'] = 'attachment; filename=texput.pdf'
# pdf = buffer.getvalue()
    # buffer.close()
    # response.write(pdf)
    # buffer.close()
    r.write(pdf)
    return r


class ListVisites(LoginRequiredMixin, ListView):
    model = Visite

    context_object_name = 'visite_list'

class DetailVisite(LoginRequiredMixin, DetailView):
    model = Visite
    context_object_name = 'visite'
    def get_context_data(self, **kwargs):
    # Call the base implementation first to get a context
        context = super(DetailVisite, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['patient'] = Patient.objects.filter(name=self.object)
        return context


class CreateVisite(LoginRequiredMixin, CreateView):
    model = Visite
    fields = '__all__'
    success_url = reverse_lazy('list_visites')

### views.py
   # from .models import Patient,Admission
   # from intervention.models import Coronarographie,Stimulation
   # from django.shortcuts  import render_to_response,redirect

# QUERY="search-query"

# MODEL_MAP = { Patient: ["name","birth","phone",],
#                  Admission: ["admission_date", "motif", "resume"],
#                  Coronarographie: ["patient", "intervention_date", "conclusion", "decision","procedure"],
#                  Stimulation  : ["intervention_date", "indication","patient"],

#    }

# def search(request):


#     objects = []
    
#     for model,fields in MODEL_MAP.items():

#         objects+=generic_search(request,model,fields,QUERY)

#         return render_to_response("benzerdjeb/search_results.html",
#                                  {"objects":objects,
#                                   "search_string" : request.GET.get(QUERY,""),
#                               }
#        )


class PatientSearchListView(ListPatients):
    """
    Display the patient List page filtered by the search query.
    """
    paginate_by = 10
    
    def get_queryset(self):
        result = super(PatientSearchListView, self).get_queryset()
        
        query = self.request.GET.get('q')
        if query:
            query_list = query.split()
            result = result.filter(
                reduce(operator.and_,
                       (Q(name__icontains=q) for q in query_list)) |
                reduce(operator.and_,
                       (Q(phone__icontains=q) for q in query_list))
                # reduce(operator.and_,
                       # (Q(tags__icontains=q) for q in query_list))
            )

        return result
