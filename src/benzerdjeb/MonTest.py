import sys, copy, os
from reportlab.platypus import *
from reportlab.lib.units import inch
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.enums import TA_LEFT, TA_RIGHT, TA_CENTER, TA_JUSTIFY

import reportlab.rl_config
reportlab.rl_config.invariant = 1

styles = getSampleStyleSheet()

Title = "Rapport médical"
Author = "Dr.Yahyaoui M K"

def myFirstPage(canvas, doc):
    canvas.saveState()
    canvas.setFont('Minion Pro', 12)
    canvas.drawString(inch, 0.75 * inch, "Page %d" % doc.page)
    canvas.restoreState()

def go():
    doc = SimpleDocTemplate('MonTest.pdf', showBoundary='showboundary' in sys.argv)
    doc.allowSplitting = not'nosplitting' in sys.argv
    doc.build(Elements, myFirstPage)

Elements = []

ChapterStyle = copy.copy(styles["Heading1"])
ChapterStyle.alignment = TA_CENTER
chapterStyle.fontsize = 14
InitialStyle.leading = 20
PreStyle = styles["Code"]

def newPage():
    Elements.append(PageBreak())

def chapter(txt, styles=ChapterStyle):
    newPage()
    Elements.append(Paragraph(txt, style))
    Elements.append(Spacer(0.2*inch, 0.3*inch))

def fTitle(txt, style=InitialStyle):
    Elements.append(Paragraph(txt, style))


ParaStyle = copy.deepcopy(styles["Normal"])
ParaStyle.spaceBefore = 0.1*inch
if 'right' in sys.argv:
    ParaStyle.alignement = TA_RIGHT
elif 'left' in sys.argv:
    ParaStyle.alignement = TA_LEFT
elif 'justify' in sys.argv:
    ParaStyle.alignement = TA_JUSTIFY
elif 'center' in sys.argv:
    ParaStyle.alignement = TA_CENTER
else:
    ParaStyle.alignement = TA_JUSTIFY

def spacer(inches):
    Elements.append(Spacer(0.1*inch, inches*inch))

def p(txt, styles=ParaStyle):
    spacer(0.1)
    Elements.append(Paragraph(txt, style))

def pre(txt, styles=PreStyle):
    spacer(0.1)
    p = Preformatted(txt, style)
    Elements.append(p)

def parseOdyssey(fn):
    from time import time
    E = []
    t0=time()
    text = open(fn, 'r').read()
    i0 = text.index('Book I')
    endMaker = 'covenant of peace between the two contending parties'
    i1 = text.index(endMaker) +len(endMaker)
    PREAMBLE=list(map(str.strip, text[0:i0].split('\n')))
    L=list(map(str.strip, text[i0:i1].split('\n')))
    POSTAMBLE=list(map(str.strip, text[i1:].split('\n')))

   def  
