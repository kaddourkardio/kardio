from django.contrib import admin

from . import models
from django_markdown.admin import MarkdownModelAdmin

#from .models import Patient
#from .models import Admission
#from .models import Wilaya
#from .models import Motif

# Register your models here.
class PatientAdmin(MarkdownModelAdmin):

        list_display = ("name","birth","ardress", "phone", "first_admission")
        list_filter = ('ardress', 'gender', 'assured')
        search_fields = ('name','phone')

class AdmissionAdmin(MarkdownModelAdmin):

        list_display = ("patient", "medecin", "admission_date", "emergency", "date_sortie", "sortant")
        list_filter = ('admission_date', 'emergency', 'medecin','patient',)


admin.site.register(models.Patient, PatientAdmin)
admin.site.register(models.Motif)
admin.site.register(models.Wilaya)
admin.site.register(models.Admission, AdmissionAdmin)
admin.site.register(models.Tag)
admin.site.register(models.Biology)
# Register your models here.
admin.site.register(models.Visite)
