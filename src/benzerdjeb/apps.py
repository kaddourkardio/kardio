from django.apps import AppConfig


class BenzerdjebConfig(AppConfig):
    name = 'benzerdjeb'
