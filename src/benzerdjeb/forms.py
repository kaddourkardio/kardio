# modelform pour me faciliter la vie
from django.forms import ModelForm
from benzerdjeb.models import Patient, Admission, Biology

class PatientForm(ModelForm):

    class Meta:
        model = Patient
        #exclude = ['','']
        fields = '__all__'

class AdmissionForm(ModelForm):

    class Meta:
        model = Admission
        exclude = ['duration']


class BiologyForm(ModelForm):

    class Meta:
        model = Biology
        fields = '__all__'
