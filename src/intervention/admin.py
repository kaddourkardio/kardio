from django.contrib import admin
from . import models
from django_markdown.admin import MarkdownModelAdmin
# Register your models here.
class CoronarographieAdmin(MarkdownModelAdmin):

        list_display = ("patient","intervention_date","operateur", "emergency")
        list_filter = ('patient', 'intervention_date', 'operateur')
#        search_fields = ('procedure','decision')

class StimulationAdmin(MarkdownModelAdmin):

        list_display = ("patient","intervention_date","operateur", "emergency")
        list_filter = ('patient', 'intervention_date', 'operateur')
#        search_fields = ('name','phone')

admin.site.register(models.Coronarographie, CoronarographieAdmin)
admin.site.register(models.Stimulation, StimulationAdmin)
admin.site.register(models.Lesion)
admin.site.register(models.Indication)
admin.site.register(models.Complication)
admin.site.register(models.Guidwire)
admin.site.register(models.Ballon)
admin.site.register(models.Stent)
admin.site.register(models.Pace)
#admin.site.register(models.Patient, PatientAdmin)
admin.site.register(models.Sonde)
