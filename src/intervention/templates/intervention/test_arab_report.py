from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter, A4
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont   

def GenerujPustyArkusz(c):
    pdfmetrics.registerFont(TTFont('Amiri', 'Vera.ttf'))
    c.setFont("Amiri", 8)
    s = u"مصلحة أمراض القلب} "
    c.drawString(450,750, s)   

def test():
    c = canvas.Canvas("test.pdf", pagesize=letter)
    GenerujPustyArkusz(c)
    c.showPage()
    c.save()  

test()
