(TeX-add-style-hook
 "pace"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt" "a4paper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("xcolor" "usenames" "dvipsnames" "svgnames" "table") ("geometry" "a4paper" "left=2cm" "right=2cm" "top=2cm" "bottom=2cm")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art12"
    "xcolor"
    "graphicx"
    "xltxtra"
    "xunicode"
    "fontspec"
    "geometry"
    "fancyhdr"
    "polyglossia"
    "bidi")
   (TeX-add-symbols
    "tightlist")
   (LaTeX-add-polyglossia-langs
    '("french" "defaultlanguage" "")
    '("arabic" "otherlanguage" "")))
 :latex)

