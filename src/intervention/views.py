from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy
from intervention.models import Coronarographie, Stimulation
from benzerdjeb.models import Patient
from django.views.generic import ListView, DetailView, TemplateView, CreateView, UpdateView
# from intervention.forms import StimulationForm, CoronarographieForm
from django.http import HttpResponse
from django.template import Context
from django.template.loader import get_template
from braces.views import LoginRequiredMixin, GroupRequiredMixin
from subprocess import Popen, PIPE
import tempfile
import os

# Create your views here.


class ListCoronarographies(LoginRequiredMixin, ListView):
    model = Coronarographie
    context_object_name = 'list_coronarographies'

class DetailCoronarographie(LoginRequiredMixin, DetailView):
    model = Coronarographie
    context_object_name = 'coronarographie'

class CreateCoronarographie(GroupRequiredMixin, CreateView):
    model = Coronarographie
    fields = '__all__'
    group_required = u'Cardiologues'
 #   success_url = reverse_lazy('list_coronarographies')
 #   success_url = reverse_lazy('coronarographie', pk = 'pk')
    success_url = reverse_lazy('list_coronarographies')

class UpdateCoronarographie(GroupRequiredMixin, UpdateView):
    model = Coronarographie
    fields = '__all__'
    group_required = u'Cardiologues'
    success_url = reverse_lazy('list_coronarographies')

class ListStimulations(LoginRequiredMixin, ListView):
    model = Stimulation
    context_object_name = 'list_stimulations'

class DetailStimulation(LoginRequiredMixin, DetailView):
    model = Stimulation
    context_object_name = 'stimulation'

class CreateStimulation(GroupRequiredMixin, CreateView):
    model = Stimulation
    fields = '__all__'
    group_required = u'Cardiologues'
    success_url = reverse_lazy('list_stimulations')
   # success_url = 'list_stimulations'


class UpdateStimulation(GroupRequiredMixin, UpdateView):
    model = Stimulation
    fields = '__all__'
    group_required = u'Cardiologues'
    success_url = reverse_lazy('list_stimulations')

def entry_as_pdf(request, pk):
    entry = Coronarographie.objects.get(pk=pk)
    context = Context({ 'coronarographie': entry })
    template = get_template('intervention/coro.tex')
    rendered_tpl = template.render(context, request).encode('utf-8')
    # Python3 only. For python2 check out the docs!
    with tempfile.TemporaryDirectory() as tempdir:
        # Create subprocess, supress output with PIPE and
        # run latex twice to generate the TOC properly.
        # Finally read the generated pdf.
        for i in range(2):
            process = Popen(
                ['xelatex', '-output-directory', tempdir],
                stdin=PIPE,
                stdout=PIPE,
            )
            process.communicate(rendered_tpl)
        with open(os.path.join(tempdir, 'texput.pdf'), 'rb') as f:
            pdf = f.read()
    r = HttpResponse(content_type='application/pdf')
    # r['Content-Disposition'] = 'attachment; filename=texput.pdf'
    r.write(pdf)
    return r

def stimulation_pdf(request, pk):
    entry = Stimulation.objects.get(pk=pk)
    # source = Patient.objects.get(pk=pk)
    # context = Context({ 'stimulation': entry, 'patient': source })

    context = Context({ 'stimulation': entry })
    template = get_template('intervention/pace.tex')
    rendered_tpl = template.render(context, request).encode('utf-8')
    # Python3 only. For python2 check out the docs!
    with tempfile.TemporaryDirectory() as tempdir:
        # Create subprocess, supress output with PIPE and
        # run latex twice to generate the TOC properly.
        # Finally read the generated pdf.
        for i in range(2):
            process = Popen(
                ['xelatex', '-output-directory', tempdir],
                stdin=PIPE,
                stdout=PIPE,
            )
            process.communicate(rendered_tpl)
        with open(os.path.join(tempdir, 'texput.pdf'), 'rb') as f:
            pdf = f.read()
    r = HttpResponse(content_type='application/pdf')
    # r['Content-Disposition'] = 'attachment; filename=texput.pdf'
    r.write(pdf)
    return r
