Django==1.9.1
# django-admin-bootstrapped==2.5.7
django-authtools==1.4.0
django-braces==1.8.1
django-crispy-forms==1.6.0
django-debug-toolbar==1.4
django-environ==0.4.0
django-markdown==0.8.4
# django-vanilla-views==1.0.4
easy-thumbnails==2.3
Markdown==2.6.5
# markdown2==2.3.0
Pillow==3.1.0
psycopg2==2.6.1
six==1.10.0
sqlparse==0.1.18
Werkzeug==0.11.3
wheel==0.24.0
